#!/usr/bin/env bash

# see https://docs.gitlab.com/ce/api/project_level_variables.html#create-variable for api-description

if [ "$#" -ne 2 ]; then
    echo "usage: ./`basename "$0"` <your gitlab project namespace> <your gitlab profile private token with api scope>"
    echo "example: ./`basename "$0"` sarstedt/adia/bookingservice 8uFL9uZnrFsB7rtGkDhU"
    exit 1
fi

# url-encoded namespace of your project: https://www.urlencoder.org/
# see https://stackoverflow.com/questions/296536/how-to-urlencode-data-for-curl-command
rawurlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02x' "'$c"
     esac
     encoded+="${o}"
  done
  echo "${encoded}"    # You can either set a return variable (FASTER)
  REPLY="${encoded}"   #+or echo the result (EASIER)... or both... :p
}
NAMESPACE=$( rawurlencode "$1" )
REPOSITORY_URL="https://gitlab.com/api/v4/projects/$NAMESPACE/variables"
# set your private access token from Profile->Settings->Access Tokens (Scope "api" is needed) here
PRIVATE_TOKEN="$2"

echo !!! DO NOT check in this file to git! See https://12factor.net/config !!!
echo Saving variables in your gitlab project with namespace \"$NAMESPACE\"...

set_variable () {
    echo "Setting variable $1 to value $2"
    curl --request DELETE --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "$REPOSITORY_URL/$1" 1> /dev/null 2> /dev/null
    curl --fail --request POST --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" $REPOSITORY_URL --form "key=$1" --form "value=$2"
    if [ "$?" -ne 0 ]; then
        echo "Error storing variable in gitlab. Did you provide correct parameters?"
        exit 1
    fi
    echo
}

PROPERTIES=$(
while read line
do
    if [[ ${line} == \#* ]]; then
        continue
    fi

    PROPERTY=`echo -n "${line}" | sed 's/\([^=]*\)=.*/\1/'`
    echo -n "$PROPERTY "
done < gradle.properties)

VARS=($PROPERTIES)
for i in "${VARS[@]}"
do
   :
   VALUE=`grep $i gradle.properties | sed 's/[^=]*=\(.*\)/\1/'`
   set_variable "ORG_GRADLE_PROJECT_$i" $VALUE
done

echo Done.

