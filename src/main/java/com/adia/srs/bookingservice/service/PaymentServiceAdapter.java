package com.adia.srs.bookingservice.service;

import com.adia.srs.bookingservice.domain.dtos.CreditCheckDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

import static org.springframework.http.RequestEntity.get;

@Service
public class PaymentServiceAdapter {

    private final String paymentServiceURL;

    private final RestTemplate restTemplate;

    @Autowired
    public PaymentServiceAdapter(RestTemplate restTemplate,
                                 @Value("${PAYMENTSERVICE_URL:http://paymentservice/checkCredit?customerId=%d}") String paymentServiceURL) {
        this.paymentServiceURL = paymentServiceURL;
        this.restTemplate = restTemplate;
    }

    public CreditCheckDTO checkCredit(long customerId) {
        URI url = URI.create(String.format(paymentServiceURL, customerId));
        RequestEntity<Void> request = get(url).accept(MediaType.APPLICATION_JSON).build();

        return restTemplate.exchange(request, CreditCheckDTO.class).getBody();
    }
}
