package com.adia.srs.bookingservice.domain.entities;

import com.adia.srs.bookingservice.domain.dtos.BookingDTO;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class Booking {

    @Setter(AccessLevel.NONE)
    private final Date createdOn = new Date();
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String ferry;

    private Date lastUpdatedOn;

    @Setter(AccessLevel.NONE)
    private BookingStatus bookingStatus = BookingStatus.REQUESTED;

    @Setter(AccessLevel.NONE)
    private CheckInStatus checkinStatus = CheckInStatus.PRE_CHECKIN;

    public Booking(String ferry) {

        this.ferry = ferry;
        this.lastUpdatedOn = new Date();
    }

    public static Booking of(BookingDTO bookingDTO) {
        return new Booking(bookingDTO.ferry);
    }

    public void updateBookingStatus(BookingStatus newStatus) {
        bookingStatus = bookingStatus.transition(newStatus);
        lastUpdatedOn = new Date();
    }

    public void updateCheckInStatus(CheckInStatus newStatus) {
        checkinStatus = checkinStatus.transition(newStatus);
        lastUpdatedOn = new Date();
    }
}
