package com.adia.srs.bookingservice.domain.entities;

public enum Gender {
    MALE, FEMALE, OTHER, UNKNOWN
}
