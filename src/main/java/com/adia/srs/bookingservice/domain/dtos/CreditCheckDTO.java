package com.adia.srs.bookingservice.domain.dtos;

import lombok.*;

@Data
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
public class CreditCheckDTO {
    private boolean creditOk;
}
