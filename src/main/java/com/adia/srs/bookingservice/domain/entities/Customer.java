package com.adia.srs.bookingservice.domain.entities;

import com.adia.srs.bookingservice.domain.dtos.CustomerCreateDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ApiModelProperty(required = true)
    private String firstName;

    @ApiModelProperty(required = true)
    private String lastName;

    @ApiModelProperty(required = true)
    private Gender gender;

    @ApiModelProperty(required = true)
    private String email;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Setter(AccessLevel.NONE)
    private List<Booking> bookings = new ArrayList<>();

    public Customer(String firstName, String lastName, Gender gender, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
    }

    public static Customer of(CustomerCreateDTO customerCreateDTO) {
        return new Customer(customerCreateDTO.getFirstName(), customerCreateDTO.getLastName(),
                customerCreateDTO.getGender(), customerCreateDTO.getEmail());
    }

    public void addBooking(Booking booking) {
        this.bookings.add(booking);
    }
}
