package com.adia.srs.bookingservice.gateway;

import com.adia.srs.bookingservice.domain.events.CheckInStatusChangedEvent;
import com.adia.srs.bookingservice.exceptions.BookingNotFoundException;
import com.adia.srs.bookingservice.service.CustomerService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

/**
 * Implements the Messaging Gateway Pattern
 *
 * @see <a href="https://www.enterpriseintegrationpatterns.com/patterns/messaging/MessagingGateway.html">Messaging Gateway Pattern</a>
 */
@Service
public class MessagingGateway {

    private final CustomerService customerService;
    private final Log log = LogFactory.getLog(getClass());
    @Autowired
    private Source source;

    @Autowired
    public MessagingGateway(CustomerService customerService) {
        this.customerService = customerService;
    }

    @StreamListener(
            target = Processor.INPUT,
            condition = "headers['type']=='CheckInStatusChangedEvent'")
    public void handle(CheckInStatusChangedEvent checkInStatusChangedEvent) throws BookingNotFoundException {
        log.info("CheckInStatusChangedEvent received");

        customerService.handle(checkInStatusChangedEvent);
    }

    public <T> boolean publish(T message) {
        return source.output().send(MessageBuilder
                .withPayload(message)
                .setHeader("type", message.getClass().getSimpleName())
                .build());
    }
}
