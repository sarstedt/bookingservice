package com.adia.srs.bookingservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EntityInvalidException extends Exception {

    public EntityInvalidException(String errorMessage) {
        super("Invalid entity. Error(s): " + errorMessage);
    }
}