package com.adia.srs.bookingservice.consumerdrivencontract.producer;

import com.adia.srs.bookingservice.Application;
import com.adia.srs.bookingservice.domain.dtos.BookingDTO;
import com.adia.srs.bookingservice.domain.entities.*;
import com.adia.srs.bookingservice.domain.repositories.BookingRepository;
import com.adia.srs.bookingservice.domain.repositories.CustomerRepository;
import com.adia.srs.bookingservice.exceptions.CustomerNotFoundException;
import com.adia.srs.bookingservice.service.CustomerService;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.verifier.messaging.MessageVerifier;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureMessageVerifier
@ActiveProfiles(profiles = "testing")
public abstract class MessagingTestsBase {

    @Autowired
    MessageVerifier messaging;

    @Autowired
    private CustomerService customerService;

    @MockBean
    private BookingRepository bookingRepository;

    @MockBean
    private CustomerRepository customerRepository;

    private Customer customer;
    private Booking bookingA;
    private Booking bookingB;

    @Before
    public void setup() throws Exception {
        customer = new Customer("Stefan", "Sarstedt", Gender.MALE, "stefan.sarstedt@haw-hamburg.de");
        customer.setId(1L);
        bookingA = new Booking("Aida");
        bookingA.setId(1L);
        customer.addBooking(bookingA);
        bookingB = new Booking("Mein Schiff 400");
        bookingB.setId(2L);
        bookingB.updateCheckInStatus(CheckInStatus.CHECKED_IN);
        customer.addBooking(bookingB);

        given(customerRepository.findById(any(Long.class))).willReturn(Optional.of(customer));
        given(customerRepository.save(any(Customer.class))).willReturn(customer);

        given(bookingRepository.findById(1L)).willReturn(Optional.of(bookingA));
        given(bookingRepository.findById(2L)).willReturn(Optional.of(bookingB));
        given(bookingRepository.save(any(Booking.class))).willReturn(bookingA);

        // clear remaining messages
        this.messaging.receive("bookingservice-out", 100, TimeUnit.MILLISECONDS);
    }

    // We create a booking and therefore trigger a BookingCreatedEvent-message being sent
    protected void testBookingCreated() throws CustomerNotFoundException {
        customerService.addBooking(customer.getId(), new BookingDTO("QM2"));
    }

    // We trigger a status-update in our service,
    // which should result in a BookingStatusChangedEvent-message being sent
    protected void testBookingStatusChanged() {
        customerService.updateBookingStatus(bookingA, BookingStatus.ACCEPTED);
    }
}