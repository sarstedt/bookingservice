package com.adia.srs.bookingservice.consumerdrivencontract.producer;

import com.adia.srs.bookingservice.Application;
import com.adia.srs.bookingservice.domain.entities.Customer;
import com.adia.srs.bookingservice.domain.entities.Gender;
import com.adia.srs.bookingservice.domain.repositories.CustomerRepository;
import com.adia.srs.bookingservice.facade.BookingController;
import com.adia.srs.bookingservice.facade.CustomerController;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ActiveProfiles(profiles = "testing")
public abstract class ControllerTestsBase {

    @Autowired
    CustomerController customerController;

    @Autowired
    BookingController bookingController;

    @MockBean
    private CustomerRepository customerRepository;

    @Before
    public void setup() {
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer(null, "Stefan", "Sarstedt", Gender.MALE,
                "stefan.sarstedt@haw-hamburg.de", null));

        given(customerRepository.findAll()).willReturn(customers);

        RestAssuredMockMvc.standaloneSetup(
                customerController,
                bookingController);
        RestAssuredMockMvc.basePath = "";
    }
}